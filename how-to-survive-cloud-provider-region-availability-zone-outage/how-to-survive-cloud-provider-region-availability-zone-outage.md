
# Infrastructure design principles and best practices for high availability

- **Points to consider before we start:**

* **Understand the business requirements.**
+ Business critical application: Chance that will decrease brand value, Bad user experience. How much revenue loss is impacted by 5-minute downtime.
Creating a higly available applciation is costly so there should be a business justification for that. 
Like if application is very critical or a downtime can cause very bad user experience or if there is huge revenue loss if application is down for 5 minute. Think if amazon is down for 5 min how it will impact the revenue of Amzaon.

+ Weigh the requirement against cost and complexity:  Creating a highly available application is difficult, time-consuming and expensive.
Creating a highly availabe application that can survice availability zone outage is difficult, time-consuming and developement, operation and maintainence of such application is very a complex task.
Devlopment cost and operation complexity of highly available application is lot more than a standalone application.

* **Data consistency requirements:**
CAP theorem to get high availability in distributed systems you need to relax consistency requirements. Across regions always use async replication.
To achieve high availability we create pools of asynchronously replicated and redundant read data. 
As per cap theorem, in distributed system we can choose only one between consistency and availability. So to get high availability  you need to relax consistency requirements.

* **Internal and third-party dependency **
Your internal and third-party dependency should also be highly available. like payment/login


- **Design principles and best practices**
Now we will cover our main section. Before we start I want you to know few numbers related to availabiltiy.
* **Spread across availability zones/regions/cloud provider:**
+ Cloud provider: AWS, Azure, GCP
Object storage	Simple Storage Service (S3)	Azure Blob Storage	Google Cloud Storage
+ Region: Regions are separate geographic areas that cloud provider uses to house its infrastructure. A complete regional infrastructure outage is extremely unlikely and this gives us the greatest possible fault tolerance option in a public cloud.
	us-west-2	US West (Oregon)
	ap-south-2	Asia Pacific (Hyderabad)
	ap-south-1	Asia Pacific (Mumbai)
+ Availability zone:  Availability Zones are distinct locations within an region that are engineered to be isolated from failures in other Availability Zones. The code for Availability Zone is its Region code followed by a letter identifier.
	us-west-2a

Amazon’s SLA for its AWS products is “only” 99.9% per AZ, so by utilizing two availability zone it is possible to increase your uptime to 99.9999%. 

This is difficult, time-consuming, and expensive – so it doesn’t make sense for most of us. But for some of us, it’s a requirement.) AWS continues to put distance between itself and their competitors, grow its platform and build services and interfaces that aren’t trivial to replicate, but if your stuff is that critical, you probably have the dough
You must build share-nothing services that span AZs at a minimum.
Amazon’s SLA for its AWS products is “only” 99.9% per AZ, so by utilizing two availability zone it is possible to increase your uptime to 99.9999%. 

* No cross region/az dependency: share-nothing approach. They must access any resource locally in Region. This includes resources like S3, SQS, etc. This means several applications that are publishing data into an S3 bucket, now have to publish the same data into multiple regional S3 buckets. there should not be any cross-regional calls on the user’s call path. Data replication should be asynchronous.


Availability	        Downtime Per Year	
90% ("One Nine")	36.5 Days	
99% ("Two Nines")	3.65 Days	
99.9% ("Three Nines")	8.76 Hours	
99.99% ("FourNines")	52.56 Minutes	
99.999% ("Five Nines")	5.26 Minutes	
99.9999% ("Six Nines")	31.5 Seconds



* Loosely coupled system: Individual systems that are independent enough from each other to sustain partial outages but keep service online

* Unit of failure is single host: By building simple services composed of a single host, rather than multiple dependent hosts, one can create replicated service instances that can survive host failures.
Remote Block storage: Elastic Block Store (EBS), Azure Disk Storage, Google Persistent Disks. Ephemeral OS disks are created on the local virtual machine (VM) storage and not saved to the remote Azure Storage. Instead, focus on utilizing the ephemeral disks present on each EC2 host for persistence.  If an ephemeral disk fails, that failure is scoped to that host. Ephemeral OS disks work well for stateless workloads, where applications are tolerant of individual VM failures but are more affected by VM deployment time or reimaging of individual VM instances.

* Short timeout and quick retries: By running multiple redundant copies of each service, one can use quick timeouts and retries to route around failed or unreachable services.
* Idempotent service interfaces: safe to retry failed requests. 
Idmepotency: the result of a successfully performed request is independent of the number of times it is executed.
* Small stateless services: all data/state replication needs to be handled in the data tier.

Spreading across multiple Regions and providers adds crazy amounts of extra complexity, and complex systems tend to be less stable. Avoid this unless you really know what you’re doing. Often redundancy has a serious development and operation cost.
* Thundering herd problem: Ability to define a maximum traffic level at any point in time, so that any additional requests will be automatically shed (response == error), to protect downstream services against a thundering herd of requests. Such ability is necessary to protect services that are still scaling up to meet growing demands, or when regional caches are cold so that the underlying persistence layer does not get overloaded with requests.

* Simulate outage: Make sure to test your application in a simulated outage scenario in a test, stage or even a production environment.An untested disaster recovery strategy is equal to no disaster recovery strategy. To simulate outage there are multiple tools also avilable like chaos monkey from netflix.


# References:
* https://www.infoq.com/news/2011/04/twilio-cloud-architecture/
* https://don.blogs.smugmug.com/2011/04/24/how-smugmug-survived-the-amazonpocalypse/
* https://netflixtechblog.com/active-active-for-multi-regional-resiliency-c47719f6685b
* http://stu.mp/2011/04/the-cloud-is-not-a-silver-bullet.html
* http://stu.mp/2011/05/howto-spend-your-investors-money.html
* https://www.infoq.com/presentations/twilio-devops/
* https://www.youtube.com/watch?v=ilgpzlE7Hds



